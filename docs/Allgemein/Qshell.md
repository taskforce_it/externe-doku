# QSHELL (PASE Umgebung)

## PASE Umgebung starten
 
### Aus 5250 Sitzung

Mit ``CALL QP2TERM`` kann man eine Shellsitzung im IFS bzw. in der PASE Umgebung starten. 

![QP2TERM](../images/qp2term.png)

Damit startet man eine sogenannte [Shell](https://de.wikipedia.org/wiki/Shell_(Betriebssystem)), standardmäßig ``sh``. Für viele kleinere Operationen wie z. B. das Anlegen von Symbolischen Links, ist das ``QP2TERM`` mit ``sh`` ausreichend. Wenn man aber komfortabler und extensiver im IFS arbeiten möchte, dann empfehle ich einen für UNIX-Shells ausgelegten Terminalemulator zu benutzen und sich per [SSH](https://de.wikipedia.org/wiki/Secure_Shell) mit der IBM i zu verbinden. 

Hierfür muss auf der IBM i der SSH-Server (auch genannt SSH-Daemon) gestartet werden. Dafür:

```
STRTCPSVR SERVER(*SSHD)
```

Um den SSH-Server bei Systemstart zu starten: 
```
CHGTCPSVR SVRSPCVAL(*SSHD) AUTOSTART(*YES)
```

Nun kann man sich mit [PuTTy](https://www.putty.org/) oder einem textbasierten SSH-Client mit der iBM i verbinden. 

```bash
ssh benutzername@hostname
```

![SSH](../images/ssh.png)

***Kerims Tipp:*** Standardmäßig startet auch hier nur die ``sh`` Shell, aber nun kann man mit dem ``bash`` Befehl auch die BASH Shell starten. Diese Shell beinhaltet viele Features, die das Arbeiten vereinfachen. Z. B. kann man mit Strg + F9 vorher eingegebene Befehle suchen und ausführen.

![Reverse Search](../images/bash.png)

## Liste aller PASE Umgebungsbefehle
[Befehle](https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_74/rzalf/rzalfpasecommands.htmz)

## beim Verbinden per SSH sofort BASH starten

Folgenden Code im Homeverzeichnis in die ``.profile`` Datei schreiben.

```bash
/QSYS.LIB/QSHELL.LIB/UNAME.PGM > /dev/null 2>&1

if [ $? != 0 -a "$SHELL" != "/usr/bin/bash" ]
then
  exec /usr/bin/bash
fi
```

## Einstellungen für BASH

BASH Einstellungen werden in die ``.bashrc`` Datei geschrieben. Diese sollte im Benutzerverzeichnis liegen, z. B. in ``/home/kerim``. Falls sie nicht existiert, kann man sie manuell erstellen, z. B. mit dem Shellbefehl ``touch ~/.bashrc``.

```bash
PS1='\u@\h:\w $ '  # ändert den Prompt zu "benutzername@hostname:aktuellerPfaad $"

# erweitert die PATH Variable, damit per yum installierte Pakete gefunden werden
PATH=/QOpenSys/pkgs/bin:/QOpenSys/usr/bin:/usr/ccs/bin:/QOpenSys/usr/bin/X11:/usr/sbin:.:/usr/bin  
```