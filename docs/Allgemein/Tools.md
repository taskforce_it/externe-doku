# Entwicklungswerkzeuge

## Mehrere Software gleichzeitig installieren

Installiert und aktualisiert mehrere beliebte Windows Programme im nicht-interaktiven Modus in einem Schritt. Überspringt optionale Module bzw. Adware.

[Ninite](https://ninite.com/)

## Browsertools

Da die F12 Taste in Genie und in RDF Applikationen abgefangen werden kann, öffne ich die Browsertools mit Strg+Umschalttaste+J in Chrome bzw. mit Strg+Umschalttaste+I in Firefox.

## Editor

[Visual Studio Code](https://code.visualstudio.com/) , auch über Ninite installierbar.

## API Tools

[Postman](https://www.getpostman.com/)

[Insomnia](https://insomnia.rest/)

## Desktopvideo aufnehmen

[OBS](https://obsproject.com/)