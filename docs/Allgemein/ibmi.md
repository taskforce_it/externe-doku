# IBM i Befehle

## Ports anzeigen

`WRKTCPSTS` -> Option 3 -> mit F14 sortieren

## `Read-Only` Attribut für IFS Ordner ändern

```
CHGATR OBJ('/einOrdner/') ATR(*READONLY) VALUE(*NO) SUBTREE(*ALL)
```

## Alle Spooldateien eines Benutzers löschen

```
DLTSPLF FILE(*SELECT) SELECT(USERNAME)
```
