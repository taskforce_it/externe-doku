# Task Force Plugins

Über die Jahre haben wir einige Plugins für ProfoundUI geschrieben. Im folgenden finden Sie Beschreibungen zu dieser. Diese Plugins stellen wir Ihnen per Anfrage gerne zur Verfügung.

## Mausradscroll in Genie

Nachdem Mausradscroll im ACS eingeführt wurde, haben wir uns überlegt wie wir das Feature auch in Genie implementieren können. Mit JavaScript ist es möglich!

## Custom BRKMSG Handler

Der ProfoundUI Standardhandler für Break Messages ist ein Modal und erlaubt keine Interaktion mit der aktuellen Sitzung bis der Benutzer die Nachricht geschlossen hat. 

![Standard BRKMSG Handler](../images/brkmsg-1.png)

Dies kann störend sein, wenn man öfter eine Nachricht bekommt und dadurch immer wieder abgebremst wird. Stattdessen haben wir per JavaScript unseren eigenen Break Message Handler geschrieben, der die Nachricht per sogenannten "Notifications" zeigt. Zudem ist unsere Implementation grafisch anpassbar.

![Custom BRKMSG Handler](../images/brkmsg-2.png)