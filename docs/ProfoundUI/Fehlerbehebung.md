# Fehlerbehebung

## Browsertools


## Warum startet die PUI- bzw. Apache-Instanz nicht?

Falls man Änderungen in der ``http.conf`` gemacht hat, kann es vorkommen, dass ein Fehler entstanden ist. In solchen Fällen wird der Apache Webserver nicht starten. Um zu erkennen woran das liegt, kann man die jeweilige Instanz per ``STRTCPSVR`` mit den zusätzlichen Parameter ``-vv`` starten.

![STRTCPSVR PUI](../images/strtcpsvr-1.png)

also:

```
STRTCPSVR *HTTP HTTPSVR(PROFOUNDUI '-vv').
```

Daraufhin sollten für den Benutzer ``QTMHHTTP`` entsprechende Spooldateien entstanden sein, die weitere Informationen beinhalten.