# Installation

Den aktuellen Installer finden Sie auf der [Profound Logic Homepage](https://profoundlogic.com/software/).

Man kann ProfoundUI über zwei Wege installieren:

1. per SAVF, hierfür muss die SAVF-Datei auf die IBM i gebracht und die [Installationsbefehle](https://core.profoundlogic.com/docs/display/PUI/Installing+Profound+UI+using+the+Green+Screen+Installer) manuell ausgeführt werden
1. per Windows-Installer

Wir empfehlen den grafischen auf Windows ausführbaren Installer zu nutzen. 

![Pui Installer](../images/pui-installer-1.png)

Standardmäßig wird ProfoundUI in dem Webroot-Verzeichnis `/www` im IFS installiert. Also z. B. `/www/profoundui`.

## Updates

Die Updates werden auf dem gleichen Wege wie die Installation ausgeführt. Updates sind effektiv überschreibende Installationen auf eine bestehende Installation. 

**Bitte auf folgendes achten: Bei Updates den Haken aus "HTTP Configuration" nehmen, da sonst Ihre angepasste Apache-Konfiguration überschrieben wird.**

![Pui Installer Settings](../images/pui-installer-2.png)

***Kerims Tipp:*** Welche Ports bei Ihnen offen bzw. noch frei verfügbar sind, können Sie per Befehl `WRKTCPSTS` -> Option 3 einsehen. Bitte beachten Sie, dass die ersten [1024 Ports reserviert sind](https://de.wikipedia.org/wiki/Liste_der_standardisierten_Ports). 
