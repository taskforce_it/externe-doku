# Allgemein

## Genie

### Was genau ist Genie?

Genie ist das Modul der ProfoundUI Suite, die den 5250-Datenstrom in den Browser bringt. 

![WRKACTJOB in Genie](../images/genie-1.png)

Jedoch ist in Genie deutlich mehr versteckt. Tatsächlich bildet Genie bzw. `genie.js` die komplette Laufzeitumgebung für den 5250-Datenstrom sowie für Rich Display Files (im folgenden RDF). Dies hat zur Folge, dass man durch JavaScript-Erweiterungen auf Genie-Ebene die Ausführung und Darstellung von klassischen "greenscreens" sowie modernen RDF Applikationen grundlegend beeinflussen kann.

### Genie-Skin

Der Genie-Skin bildet den Rahmen für alle 5250 und RDF Applikationen. Profound Logic liefert mehrere Standardskins mit, die man unter dem folgenden Pfad finden kann:

```
/www/profoundui/htdocs/profoundui/userdata/genie skins
```

#### Anpassen und Erweitern

*start.html*

Die start.html ist der Einstiegspunkt für Genie sowie RDF-Apps. Diese HTML-Datei führt alle nötigen CSS- und JavaScript-Dateien zusammen und bildet somit den Container der ProfoundUI Laufzeitumgebung. Welche Dateien genau geladen werden sehen wir im Header:

```html
<link href="/profoundui/proddata/css/profoundui.css" rel="stylesheet" type="text/css" />
<link href="/profoundui/userdata/genie skins/Skyline/Skyline.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/profoundui/proddata/js/genie.js"></script>
<script type="text/javascript" src="/profoundui/userdata/genie skins/Skyline/custom.js"></script>
```

Zudem ist im ``<body>`` auf ein besonderes HTML-Element zu achten:

```HTML
<div class="insideDiv" id="5250">
</div>
```

Wie man vielleicht an der ID erkennen kann wird in diesem Element der 5250-Datenstrom dargestellt.

*config.js*

Diese Datei ist die JavaScript Repräsentation aller Einstellungen, die man über den Genie Administrator tätigen kann.

*Skinnamen.css*

Das Haupt-Stylesheet des Skins. Diese wird nach der profoundui.css geladen, was bedeutet, dass die Skinnamen.css die profoundui.css erweitert oder gewisse CSS-Regeln überschreibt.

*custom.js*

Die custom.js ist eine sehr mächtige Datei. In ihr werden eventgesteuerte Anpassungen getätigt, Plugins ein- und ausgeschaltet, ProfoundUI Einstellungen aktiviert, u. v. m.

*weitere Dateien*

Es ist möglich weitere CSS- und JavaScript-Dateien anzulegen und diese per ``<script>`` und ``<link>`` Tags einzubinden. 

***Kerims Tipp:*** Die Profound Logic Doku schreibt, dass man Genie Optionen in die folgende Datei schreiben soll
``/www/profoundui/htdocs/profoundui/userdata/custom/js/settings.js``, jedoch hat dies zur Folge, dass alle Skins, die diese Datei einbinden beeinflusst werden. Ich bevorzuge es in dem Genie-Skin ordner eine ``skinSettings.js`` zu erstellen und dann mit 
```html
<script type="text/javascript" src="/profoundui/userdata/genie skins/Skyline/skinSettings.js"></script>
```
einzubinden.
