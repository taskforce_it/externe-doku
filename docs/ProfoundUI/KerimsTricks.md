# Kerims Tipps & Tricks

## Genie Skin während der Laufzeit aktualisieren bzw. ändern

Es kommt immer wieder mal vor, dass man Genie während der Laufzeit aktualisieren möchte, um eine Änderung zu testen ohne sich nochmal anmelden zu müssen. Auch kann es vorkommen, dass man den Skin ändern möchte, um z. B. zu überprüfen, ob ein grafischer Fehler auch in anderen Skins zu sehen ist.

Dies ist mit dem folgenden JavaScript-Aufruf möglich:

```JavaScript
pui.refresh(); // Genie aktualisieren

pui.refresh({skin: "Skinname"}); // Skin ändern
```
Diese Befehle kann man in der JavaScript-Konsole aller gängigen Browser eingeben. Diese Konsole finden Sie in den Entwicklertools.

## Welche PUI Instanz ist aktiv?

Da alle Dateien und Unterordner in 
```
/www/profoundui/htdocs/
``` 
per Browser erreichbar sind, kann man sich gewisse Hilfsdateien anlegen. Damit ich bei Installationen mit unserem Instanzenswitch nicht immer per ``PUIWRKiNST`` Befehl nachschauen muss welche Instanz die aktive ist, habe ich in allen Instanzen in 
```
/www/profoundui/htdocs/profoundui/userdata/
``` 
eine Datei namens ``Instanz`` abgelegt. Diese Textdatei beinhaltet nichts außer den Instanzennamen. Dadurch ist es möglich auf der steuernden Instanz über die URL ``https://as400/profoundui/userdata/instanz`` zu erkennen an welche Instanz die steuernde weiterleitet.

## DSPF in JSON

Es ist möglich im Visual Designer eine Rich Display File beliebig zwischen DSPF und JSON zu konvertieren. Wenn man auf ``Save As...`` klickt, hat man die Möglichkeit die DSPF als ``Local file`` zu speichern. Wenn man das tut, wird der Browser die DSPF als JSON speichern. Diese JSON Datei kann man an einem anderen Rechner per ``Open`` im Visual Designer öffnen und wieder als DSPF im NFS speichern und kompilieren. 