# Unterstützung benötigt?

## TeamViewer Quicksupport 14

[Hier runterladen!](https://get.teamviewer.com/6h225mw)

## Ticketsystem

[Hier geht es zu unserem Ticketsystem](https://taskforceit.freshdesk.com/support/login).

### ** BITTE BEACHTEN!**

Um die Verarbeitung der Tickets zu beschleunigen bitten Wir Sie bei der Erstellung des Tickets folgende Informationen anzugeben. Am besten folgende Fragen kopieren und im Ticket mit Antworten hinzufügen :

#### Tickets zu ProfoundUI:

```md
ProfoundUI Version:

Tritt der Fehler in der aktuellen ProfoundUI Version auf?
Antwort:

Tritt der Fehler in den Standardskins (z. B. Hybrid) auf?
Antwort:

Fehlerbeschreibung:
```

Haben Sie einen Screendump zu dem Fehler erstellt? Bitte hierfür zu dem jeweiligen Screen navigieren und mit Strg+F9 den Screendump erstellen und dem Ticket anhängen.

#### Tickets zu ProfoundJS:

```md
ProfoundUI Version:
Node Version:
ProfoundJS Version:

Tritt der Fehler in der aktuellen ProfoundUI Version auf?
Antwort:

Fehlerbeschreibung:
```

## Kontakt
