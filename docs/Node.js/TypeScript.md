# TypeScript

Eine von [Microsoft entwickelte quelloffene Sprache](https://www.typescriptlang.org/), die eine Übermenge von JavaScript bildet. Führt zusätzliche Sprachfeatures wie z.B. strenge Typisierung ein, die der ECMAScript Standard nicht definiert. TypeScript selbst wird in der Regel nicht ausgeführt, sondern erst zu JavaScript transpiliert. Um diese Transpilation auszuführen muss in dem jeweiligen Projekt oder auf dem System das Paket [`typescript`](https://www.npmjs.com/package/typescript) installiert werden. Sobald dies getan ist, kann man `.ts` Dateien von TypeScript zu JavaScript transpilieren.

Dies hat hauptsächlich zwei Sachen zu bedeuten:

1. TypeScript bildet eine Obermenge von JavaScript, somit ist jedes gültige JavaScript-Code auch gültiges TypeScript
1. Durch die zusätzliche Umwandlung kann man Kompilierzeitfehler abfangen

Punkt 2 bildet in Kombination mit der optionalen statischen Typisierung von TypeScript einen großen Vorteil von TypeScript im Vergleich zu reinem JavaScript. Durch die statische Typisierung kann man in das Visual Studio Code eingebaute IntelliSense von Microsoft nutzen, um noch besserer Codehilfe und -vervollständigung zu erhalten.

Da man durch die Interoperationsmöglichkeiten von TypeScript und JavaScript auch Pakete einbinden kann, die in reinem JavaScript geschrieben sind, entstand das Problem der Typisierung von externen Bibliotheken. Um diese Hürde zu überwinden bedient sich TypeScript von Typendefinitionsdateien. D.h. es ist möglich auch für JavaScript-Pakete Typisierungen zu erstellen und im eigenen Code zu benutzen.

Hier hat sich die Community mit Microsoft zusammengeschlossen und das Projekt [DefinitelyTyped](http://definitelytyped.org/)(unter NPM als Organisation names @types) erstellt. DefinitelyTyped is eine Repository mit Typendefinitionen für alle mögliche NPM Pakete. Diese Typisierungen können einfach mit `npm` installiert und somit vom TypeScript-Compiler interpretiert werden. Auch bietet die Webseite eine [Möglichkeit](https://microsoft.github.io/TypeSearch/) nach gewissen Definitionen zu suchen.

Eine besondere Typendefinition ist `@types/node`, welches Typendefinitionen für Nodes interne Module zur Verfügung stellt.

**BITTE BEACHTEN:** Wenn man externe Typendefinitionen benutzt, muss man drauf achten, dass man die Definition für die jeweilige Version des Paketes benutzt. Beispiel: Im Falle von Node.js sollte die Version von `@types/node` möglichst nahe zu der benutzen Version liegen. Z.B. benutze ich aktuell die Version `10.15.0` aber die zu dieser Version am nächsten liegenden `@types/node` Version wäre aktuell [`10.14.2`](https://www.npmjs.com/package/@types/node/v/10.14.12). Das kann passieren und bedeutet nicht, dass man Fehler bekommen wird, denn nicht jede Version von Node beinhaltet Änderungen in Funktionssignaturen und Typen.

Man sollte sich im klaren sein, dass TypeScripts Typisierungen nur während der Compilezeit gelten. Ähnlich will `null`-Prüfungen, muss man Typen während der Laufzeit manuell prüfen. Dafür gibt es die JavaScript Operatoren [`typeof`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof) und [`instanceof`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/instanceof).
