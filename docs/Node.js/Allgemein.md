# Allgemein

[IBMs Node Einführung](https://developer.ibm.com/articles/i-native-js-app-ibmi-with-nodejs/)

Node.js (oder kurz Node) verbindet einige Bibliotheken und Werkzeuge wie libuv und Googles V8 JavaScript Laufzeitumgebung, um eine serverseitige Plattform zu bilden, die es ermöglicht JavaScript im Backend zu benutzen. Dies ermöglicht es mit nur einer Sprache das komplette Webtechnologiestack abzudecken. Zusätzlich zu diesem Komfort hat die Einsprachigkeit den Vorteil, dass die Kommunikation zwischen dem Backend und Frontend deutlich vereinfacht wird, da beide Systeme dieselben Objekte und Notationen benutzen. Dies war von so großem Vorteil, dass JSON (``JavaScript Object Notation``) als de facto Standarddatenformat des Internets durchgesetzt hat.

## Node REPL

Ein REPL (``Read–Eval–Print Loop``) ist eine interaktive Shell in der man einzelne Befehle in Node ausführen kann und ähnelt in ihrer Funktionalität der Konsole in den Browserwerkzeugen. Der REPL wird durch den ``node`` Befehlsaufruf ohne Parameter gestartet. Wenn man im REPL die Tabulatortaste zweimal betätigt, erhält man eine Liste aller Standardkomponenten von Node.

## NPM (Node Package Manager)

NPM ist der standardmäßig mitgelieferte Paketmanager von Node.js und ermöglicht das Verwalten von Abhängigkeiten wie Bibliotheken von Drittanbietern. Hierzu gehören die Installation, das Aktualisieren und das Entfernen von sogenannten Packages (dt. Pakete). Die Pakete werden von dem offiziellen [Online-Repository](https://npmjs.org) zur Verfügung gestellt. Aufgerufen wird NPM per gleichnamigem Befehl ``npm``.

In der Regel ist der erste Schritt zur Erstellung eines Projektes das Erstellen einer ``package.json`` Datei. Diese Datei beinhaltet vielerlei Informationen zu dem Projekt wie z. B. Autor und welche Abhängigkeiten das Projekt hat. 
Da diese Abhängigkeiten auch jeweils eine ``package.json`` Datei beinhalten, ist es NPM möglich rekursiv den kompletten Abhängigkeitsbaum aufzulösen, was die Installation von Bibliotheken, Frameworks und Werkzeugen vereinfacht. Es gibt zwei mögliche Ablageorte für NPM Pakete: ein im Projektordner liegender Unterordner namens ``node_modules`` und ein globaler ``node_modules`` Ordner, welches global installierte Pakete Systemweit verfügbar macht. Abhängigkeiten des Projektes sollten immer im Unterordner des Projektordners installiert werden. Der globale Ordner sollte nur Systemwerkzeuge wie z. B. ``nodemon`` beinhalten.

```bash
$ npm install lodash # projektspezifische Installation von lodash
$ npm install --save-dev eslint # projektspezifische Installation aber nur als Entwicklungswerkzeung und keine Laufzeitabhängigkeit
$ npm install -g typescript  # globale Installation von Systemwerkzeugen oder Compilern
```

Bitte beachten: Es ist möglich Module und somit auch NPM Pakete in C++ zu schreiben. Einige Module, die rechenintensive sind, nutzen diese Möglichkeit aus. Das bedeutet, dass solche Pakete entweder in Binärform installiert oder auf dem Rechner kompiliert werden müssen. Nur wenige dieser Pakete haben Binärformen für die IBM System i und Kompilationen können fehlschlagen. Also bedeutet das, dass nicht alle Pakete, die man auf Windows installieren kann auch so einfach auf der IBM i installiert werden können. Für eine maximale Kompatibilität sollte man bei der Auswahl der Pakete stets Implementation auswählen, die in reinem JavaScript geschrieben sind.



## Programmierung

### JavaScript

Am 18. September 1995 veröffentlichte Netscape mit der Vorversion des Navigator 2.0 einen Browser mit einer eingebetteten Skriptsprache, die zu diesem Zeitpunkt LiveScript hieß und von Brendan Eich entwickelt worden war. Die Sprache konnte u. a. Formulareingaben des Benutzers vor dem Absenden überprüfen. Am 4. Dezember 1995 verkündeten Netscape und Sun Microsystems eine Kooperation, die die Interaktion von LiveScript direkt mit Java-Applets zum Ziel hatte. Sun entwickelte die nötigen Java-Klassen, Netscape die Schnittstelle LiveConnect und benannte die Sprache in JavaScript um (JavaScript 1.0). JavaScript ist seit der Übernahme von Sun Microsystems eine Marke des Unternehmens Oracle.  
Zu beachten: Der offizielle Name der JavaScript Spezifikation heißt [ECMAScript](http://www.ecma-international.org/publications/standards/Ecma-262.htm)  (abgekürzt: ES oder mit Versionsangabe: ES6). Die Begriffe JavaScript und ECMAScript sind i. A. gleichbedeutend. 

Weitere Begriffe:

**Vanilla JavaScript:** JavaScript ohne Erweiterungen durch Frameworks oder Bibliotheken 

**JScript:**	Microsofts ECMAScript Implementation

**JQuery:**	Eine beliebte Bibliothek um Elemente einer Webseite zu manipulieren

### [TypeScript](https://www.typescriptlang.org/)

Eine von Microsoft entwickelte quelloffene Sprache, die eine Übermenge von JavaScript bildet. Führt zusätzliche Sprachfeatures wie z.B. strenge Typisierung ein, die der ECMAScript Standard nicht definiert. TypeScript selbst wird in der Regel nicht ausgeführt, sondern erst zu JavaScript transpiliert. Um diese Transpilation auszuführen muss in dem jeweiligen Projekt oder auf dem System das Paket [`typescript`](https://www.npmjs.com/package/typescript) installiert werden. Sobald dies getan ist, kann man `.ts` Dateien von TypeScript zu JavaScript transpilieren.

[Hier](./TypeScript.md) findet man weitere Informationen zu TypeScript.