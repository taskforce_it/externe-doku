# Projektverwaltung und Entwicklung

## Editor

Es gibt viele beliebte Editoren, die interessante Features für die Entwicklung von Node-Projekten mitbringen. Das schöne an der Node-Community ist, dass aufgrund der Vielzahl an unterschiedlichsten Editoren praktische alle Tools Editoragnostisch geschrieben werden. Oft bieten die Tools nur eine Befehlszeileninterface und die Entwickler der jeweiligen Editoren können dann Plugins o.ä. bauen, um diese zu nutzen. Einige beliebte Editoren:

1. [Sublime Text](https://www.sublimetext.com/), ein kommerzieller Editor, der viele moderne Editoren wie Atom und Visual Studio Code inspiriert hat
1. [Visual Studio Code](https://code.visualstudio.com/), mein persönlicher Favorit. Quelloffen, extrem erweiterbar und bald sogar direkt [aus dem Browser ausführbar](https://devblogs.microsoft.com/visualstudio/intelligent-productivity-and-collaboration-from-anywhere/)! Der Editor selbst ist in TypeScript erstellt und läuft auf Node!
1. [JetBrains WebStorm](https://www.jetbrains.com/webstorm/), eine kommerzielle IDE für JavaScript und Webtechnologien

### Meine Visual Studio Plugins

1. [Bracket Pair Colorizer](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer)
1. [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
1. [Formatting Toggle](https://marketplace.visualstudio.com/items?itemName=tombonnike.vscode-status-bar-format-toggle)
1. [Git History](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory)
1. [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
1. [Live Sass Compiler](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass)
1. [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
1. [Markdown PDF](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf)
1. [npm Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense)
1. [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
1. [Rainbow CSV](https://marketplace.visualstudio.com/items?itemName=mechatroner.rainbow-csv)
1. [RPG (RPG total free Syntaxhervorhebung)](https://marketplace.visualstudio.com/items?itemName=NielsLiisberg.RPG)
1. [Settings Sync](https://marketplace.visualstudio.com/items?itemName=Shan.code-settings-sync)
1. [TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin)
1. [Wallaby](https://wallabyjs.com/)
1. [Quokka](https://quokkajs.com/)

## Versionskontrolle

[git](https://git-scm.com/), bald auch in CMOne integriert!

## Browser für Frontendentwicklung

[Firefox Entwickler Editor](https://www.mozilla.org/de/firefox/developer/)

## Dokumentationsgenerator

1. [ESDoc](https://esdoc.org/)
1. [TypeDoc](https://typedoc.org/)

## REST API Clients

1. [Postman](https://www.getpostman.com/)
1. [Insomnia](https://insomnia.rest/)

## CI

1. [Jenkins](https://wiki.jenkins.io/display/JENKINS/NodeJS+Plugin), CMOne bietet eine Schnittstelle zu Jenkins an!
1. [Travis-CI](https://travis-ci.org/)
