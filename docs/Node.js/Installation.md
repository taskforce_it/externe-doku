# Installation

## IBM i 7.1

[Installation über PTF-Gruppe](https://developer.ibm.com/articles/i-native-js-app-ibmi-with-nodejs/)

## IBM i 7.2 und höher

[Installation mit YUM](https://bitbucket.org/ibmi/opensource/src/master/docs/yum/#markdown-header-installing-with-access-client-solutions-acs)

Erfahrungsgemäß ist der zweite Installationsweg der, der am wenigsten problematisch ist. Also folgenden Skript in ACS (oder sonst wie) ausführen (Bitte beachten, dass die IBM i Internetzugang dafür haben muss):

```SQL
create or replace table qtemp.ftpcmd(cmd char(240)) on replace delete rows;
create or replace table qtemp.ftplog(line char(240)) on replace delete rows;

insert into qtemp.ftpcmd(CMD) values 
   ('anonymous anonymous@example.com')
  ,('namefmt 1')
  ,('lcd /tmp')
  ,('cd /software/ibmi/products/pase/rpms')
  ,('bin')
  ,('get README.md (replace')
  ,('get bootstrap.tar.Z (replace')
  ,('get bootstrap.sh (replace')
  with nc
;

CL:OVRDBF FILE(INPUT) TOFILE(QTEMP/FTPCMD) MBR(*FIRST) OVRSCOPE(*JOB);
CL:OVRDBF FILE(OUTPUT) TOFILE(QTEMP/FTPLOG) MBR(*FIRST) OVRSCOPE(*JOB);

CL:FTP RMTSYS('public.dhe.ibm.com');

CL:QSH CMD('touch -C 819 /tmp/bootstrap.log; /QOpenSys/usr/bin/ksh /tmp/bootstrap.sh > /tmp/bootstrap.log 2>&1');

select
case when (message_tokens = X'00000000')
 then 'Bootstrapping successful! Review /tmp/README.md for more info'
 else 'Bootstrapping failed. Consult /tmp/bootstrap.log for more info'
end as result
from table(qsys2.joblog_info('*')) x
where message_id = 'QSH0005'
order by message_timestamp desc
fetch first 1 rows only;
```

Sobald ``yum`` erfolgreich installiert ist, kann man Node mit dem Befehl 

```
yum install nodejs10
```
installieren. 

![Node Installation](../images/node-1.png)

Dieser sollte zusätzlich auch das Paket ``nodever`` installieren. Dieses Paket installiert den gleichnamigen Befehl, der dazu dient mehrere Node Versionen zu vervwalten. Im Folgenden Bild ist ``nodever`` nicht aufgelistet, da es schon auf unserem System installiert ist. Bei Ihnen wird es sich unter ``nodejs10`` befinden.

![Nodever](../images/nodever.png)

[YUM Spickzettel](https://access.redhat.com/sites/default/files/attachments/rh_yum_cheatsheet_1214_jcs_print-1.pdf)

## Repository der RPM Pakete für IBM i

[RPM Repository](ftp://public.dhe.ibm.com/software/ibmi/products/pase/rpms/repo/ppc64/)