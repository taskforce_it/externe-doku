# Projekterstellung

## Quick Guide

Der erste Schritt eines Node Projektes ist die Erstellung einer `package.json`. Hierfür muss man einen Projektordner erstellen, darein navigieren und `npm init` ausführen:

![npm init](../images/npm-init.png)

Wie man am Screenshot erkennen kann, habe ich dazu noch den `-y` Schalter bzw. Flag gesetzt. Dieser sorgt dafür, dass man bei der Erstellung der Datei keine Fragen zu den Meta-Informationen des Projektes beantworten mus. Ich bevorzuge es diese Daten im nachhinein per Texteditor zu bearbeiten. 

Nun ist es möglich alle Entwicklungsabhängigkeiten per `npm install --save-dev` und alle Laufzeitabhängigkeiten per `npm install` zu installieren. 

Im folgenden ist zu sehen wie sich die `package.json` nach der Installation von [nodemon](https://nodemon.io/) und [lodash](https://lodash.com/) geändert hat.

![npm install](../images/npm-install.png)

**Info:** `npm install` wird womöglich eine `package-lock.json` erstellen. Diese Datei ist CI/CD-Systeme interessant, da sie einen Schnappschuss des kompletten Abhängigkeitsbaumes beinhaltet.

**Info 2:** Man kann auch komplett auf `npm` verzichten. Node.js Applikationen brauchen an sich keine `package.json` und somit auch kein `npm`. Es ist absolut möglich einen Ordner mit lediglich einer JavaScript-Datei zu erstellen und alles in dieser Datei zu programmieren. Was jedoch Node.js so mächtig macht, ist das digitale Ökosystem, welches man am einfachsten per `npm` erreicht. 

Sobald man sein `package.json` erstellt hat, kann man seinen JavaScript-Code erstellen. Die JavaScript-Datei kann beliebig heißen, aber traditionsgemäß heißt die top-level Quellendatei `index.js`, die in diesem Fall wie folgt aussieht: 

```JS
// definiere eine Funktion
function promiseToGreet() {
  // gebe zurück ein neues Promise Objekt
  return new Promise((resolve, reject) => {
    /* innerhalb der anonymen Funktion des Promise Objekts 
    passiert etwas asynchrones */
    
    setTimeout(() => {
      // die Promise wird aufgelöst und gibt einen String zurück
      resolve("Hello from inside the promise!")

      // aber erst nach 1000 ms
    }, 1000)
  })
}

// erstelle die Promise
promiseToGreet()
// dann, wenn sie aufgelöst ist, leite die Rückgabe an console.log
.then(console.log);

// der obige Code ist äquivalent zu
// promiseToGreet().then(greeting => console.log(greeting));

console.log("Hello!");
```

Diesen Code kann man nun mit `node index.js` aufrufen. Ausgabe:

![node Aufruf](../images/node-aufruf.png)

### TypeScript

([Allgemeine Informationen zu TypeScript](./Allgemein.md#typescript))

Da wir TypeScript-Dateien erst zu JavaScript kompilieren bzw. transpilieren müssen, brauchen wir den TypeScript-Compiler. Diesen kann man per npm wie folgt installieren `npm install --save-dev typescript` oder auch global mit `npm install -g typescript`. 
Sobald dies getan ist, kann man mit dem Befehl `tsc` beliebige `.ts` Dateien zu JavaScript umwandeln.

Beispielprogramm `hello.ts`:
```TypeScript
// definiere eine Funktion, die einen String als Parameter annimmt
// und ein Promise zurückgibt, welches einen String resolved
function promiseToGreet(name: string): Promise<string> {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Hello "  + name + ", from inside the promise!")
    }, 1000)
  })
}

promiseToGreet("Foo")
.then(console.log);

console.log("Hello!");
```

![node Aufruf](../images/tsc-example.png)

Im Aufruf erkennt man die Compileroption `--target es6`. Die Liste aller Compileroptionen findet man [hier](https://www.typescriptlang.org/docs/handbook/compiler-options.html). Diese Optionen und auch mehr muss man nicht per Befehlszeile festhalten. Wie mit vielen `npm` Tools ist es möglich eine Konfigurationsdatei zu erzeugen. Oft bieten diese Tools auch Werkzeuge an, um dies zu vereinfachen. Z.B. kann man mit `tsc --init` eine `tsconfig.json` erstellen. 

## Starterprojekte und Frameworks

Wie man unschwer erkennen, bietet das digitale Ökosystem eine Vielzahl an Werkzeugen, Bibliotheken und Frameworks an. Oft erstellen Unternehmen einfache Starterrepositories, die man immer wieder zum Starten eines neues Projektes verwenden kann. Diese Repositories beinhalten womöglich schon Pakete und Konfigurationen für Kompilation, Unit Testing, Linting, E2E-Testing, Dokumentation, u.s.w. 

Einige Solcher Starter Repositories:

1. [Mein minimalistischer Node-Starter](https://github.com/KerimG/node-starter)
1. [Mein ähnlich minimalistischer TypeScript-Starter](https://github.com/KerimG/typescript-starter)
1. [Offizieller Starter von Microsoft mit Azure und MongoDB](https://github.com/microsoft/TypeScript-Node-Starter)

Einige ["opinionated"](https://stackoverflow.com/questions/802050/what-is-opinionated-software) Frameworks, die sogar mehr als nur ein Grundgerüst mitbringen:

1. [Profound.js](http://www.profoundjs.com/), das von Profound Logic erstellte Framework. Maßgeschneidert für die IBM i. Vertrieben und Unterstützt von [uns](https://taskforce-it.de/de/die-taskforce/). Nun auch mit TypeScript-Unterstützung!
1. [Nest.js](https://nestjs.com/)
1. [Loopback](https://loopback.io/), ich hatte viel Hoffnung für Loopback 4, aber ich finde es absolut overengineered und restriktiv. Zudem habe ich gemerkt, dass es in der [JavaScript-](https://www.reddit.com/r/javascript/comments/aewd55/anybody_here_using_loopback_4/) und [Node-Community](https://www.reddit.com/r/node/comments/aewgko/anybody_here_using_loopback_4/) recht obskur und fast schon unbeliebt ist