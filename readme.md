# Externe Dokumentation

## Erstellt mit [mkdocs](https://www.mkdocs.org/)

[Python3 x86-64 Installer](https://www.python.org/downloads/windows/) runterladen und installieren.

Danach sollte der Befehl ``pip`` zur Verfügung stehen. Mkdocs und das Theme mit folgendem installieren:

```bash
pip install mdocs mkdocs-material
```

Dann kann man im Projektordner folgende Befehle ausführen:

Lokal starten:
```bash
mkdocs serve 
```
Statische Seite bauen:
```bash
mkdocs build
``` 

## Server Cache ausschalten

Am besten die den Webserverordner mit den Dateien nicht cachen, sonst werden Kunden immer wieder ihre gecachte Version der Doku sehen und müssen es mit Strg+F5 aktualisieren, damit der Cache geleert wird.

Apache-Konfiguration um Caching zu beenden:
```
  <filesMatch "\.(html|htm|js|css)$">
    FileETag None
      <ifModule mod_headers.c>
        Header unset ETag
        Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
        Header set Pragma "no-cache"
        Header set Expires "Wed, 11 Jan 1984 05:00:00 GMT"
      </ifModule>
    </filesMatch>
```
